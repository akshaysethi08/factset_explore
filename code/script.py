import psycopg2
import pandas as pd 
import numpy as np 
import pickle 

conn = psycopg2.connect( database="r2", user = "postgres", password = "1234", host = "127.0.0.1", port = "8003")

# get all table names
cursor = conn.cursor()
cursor.execute("SELECT table_name FROM information_schema.tables ;")

ents = []
refs = []
syms = []

schema_name = 'fds'

tables = cursor.fetchall()

imp_tables = []

for table in tables:
	
	if (table[0][:3] ==  'ref'):
		#print (table[0])
		refs.append(table[0])

	elif (table[0][:3] == 'sym'):
		#print (table[0])
		syms.append(table[0])

	elif (table[0][:3] == 'ent'):
		#print (table[0])
		ents.append(table[0])
		

for table in tables:

	if ("sym_address" in table[0]):
		#print (table[0])
		imp_tables.append(table[0])

	elif (table[0] == "sym_v1_sym_entity"):
		#print (table[0])
		imp_tables.append(table[0])

	elif ("relationship_type_map" in table[0]):
		#print (table[0])
		imp_tables.append(table[0])

	elif ("ent_scr_parent" in table[0]):
		#print (table[0])
		imp_tables.append(table[0])

	elif ("ent_scr_relevance_rank" in table[0]):
		#print (table[0])
		imp_tables.append(table[0])
		

imp_tables = imp_tables + ents

print ("number of important tables are ",len(imp_tables))

df_dict = {}

# save multiple csvs
for i in range(len(imp_tables)-1,-1,-1):

	print (i)
	#r_num = np.random.randint(0,len(tables))
	r_num = i
	name = schema_name + '.' + imp_tables[r_num]
	sql = "SELECT * FROM %s;"%(name)
	df = pd.read_sql_query(sql, conn)
	print (df)
	print (name)
	df_dict[name] = df

	#conn = None  
	#df.to_excel("%s.xlsx"%(imp_tables[r_num]))

f = open("df_file.pkl","wb")
pickle.dump(df_dict,f)
f.close()




