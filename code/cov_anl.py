import psycopg2
import pandas as pd 
import numpy as np 
import pickle 

handle = open('scr_coverage.pkl', 'rb')
df = pickle.load(handle)

relations_sum = df["has_direct_rel"].sum()
rev_relations_sum = df["has_reverse_rel"].sum()

print (df.shape[0])
print (relations_sum , rev_relations_sum)
print (relations_sum/df.shape[0], rev_relations_sum/df.shape[0])

