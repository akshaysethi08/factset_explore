import numpy as np 
import matplotlib.pyplot as plt

x = np.load('primary.npy')

n, bins, patches = plt.hist(x, 100, facecolor='g', alpha=0.75)

plt.xlabel('percentage revenue')
plt.ylabel('Frequency')
plt.title('Histogram of % revenue')
#plt.xticks(range(0, 100))
#plt.yticks(range(0, 100))
#plt.text(60, .025, r'$\mu=100,\ \sigma=15$')
plt.axis([0, 100, 0, 12000])
#plt.grid(True)
plt.show()

print (np.mean(x))