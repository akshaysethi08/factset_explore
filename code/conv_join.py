import psycopg2
import pandas as pd 
import numpy as np 
import pickle 

handle = open('df_file.pkl', 'rb')
df_dict = pickle.load(handle)
df = (df_dict["fds.ent_v1_ent_scr_coverage"])
other = (df_dict["fds.sym_v1_sym_entity"])

df2 =  (df.set_index('factset_entity_id').join(other.set_index('factset_entity_id')))
cols = df2.columns.tolist()
cols = cols[-3:] + cols[:3]
df2 = df2[cols]
print (df2)

h2 = open('scr_coverage.pkl', 'wb')
pickle.dump(df2, h2)
h2.close()




